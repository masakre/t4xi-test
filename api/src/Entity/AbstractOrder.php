<?php

namespace Trial\CoffeeMachine\Entity;

use Symfony\Component\HttpFoundation\Response;
use Trial\CoffeeMachine\Infrastructure\MysqlPdoClient;

abstract class AbstractOrder
{
    public $drinkType;

    public $money;

    public $sugars;

    public $extraHot;

    public $price;

    public function __construct( string $drinkType,
                                 string $money,
                                 int $sugars,
                                 int $extraHot,
                                 float $price
    ) {
    }


    /**
     * @return string
     */
    public function getDrinkType(): string
    {
        return $this->drinkType;
    }

    /**
     * @param string $drinkType
     */
    public function setDrinkType( string $drinkType ): void
    {
        $this->drinkType = $drinkType;
    }

    /**
     * @return int
     */
    public function getSugars(): int
    {
        return $this->sugars;
    }

    /**
     * @param int $sugars
     */
    public function setSugars( int $sugars ): void
    {
        $this->sugars = $sugars;
    }

    /**
     * @return string
     */
    public function getExtraHot(): string
    {
        return $this->extraHot;
    }

    /**
     * @param string $extraHot
     */
    public function setExtraHot( string $extraHot ): void
    {
        $this->extraHot = $extraHot;
    }

    /**
     * @return string
     */
    public function getMoney(): string
    {
        return $this->money;
    }

    /**
     * @param string $money
     */
    public function setMoney( string $money ): void
    {
        $this->money = $money;
    }

    /**
     * @return int
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice( float $price ): void
    {
        $this->price = $price;
    }


    public function toArray(): array {
        return [
            'drinkType' => $this->drinkType,
            'money' => $this->money,
            'sugars' => $this->sugars,
            'extraHot' => $this->extraHot,
        ];
    }

    public function getStick(): int
    {
        return (int)( $this->getSugars() > 0 );
    }


}