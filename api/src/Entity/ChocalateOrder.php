<?php


namespace Trial\CoffeeMachine\Entity;


class ChocalateOrder extends AbstractOrder
{

    /**
     * ChocalateOrder constructor.
     * @param $price
     */
    public function __construct(string $money, int $sugars, string $extraHot )
    {
        $this->price = 0.6;
        $this->setDrinkType( 'chocolate' );
        $this->money = $money;
        $this->sugars = $sugars;
        $this->extraHot = $extraHot;
    }
}