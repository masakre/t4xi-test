<?php


namespace Trial\CoffeeMachine\Entity;


class CoffeeOrder extends AbstractOrder
{
    /**
     * Coffee constructor.
     * @param $price
     */
    public function __construct(string $money,int $sugars, string $extraHot )
    {
        $this->price = 0.5;
        $this->setDrinkType( 'coffee' );
        $this->money = $money;
        $this->sugars = $sugars;
        $this->extraHot = $extraHot;
    }
}