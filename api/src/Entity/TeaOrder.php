<?php


namespace Trial\CoffeeMachine\Entity;


class TeaOrder extends AbstractOrder
{

    /**
     * TeaOrder constructor.
     * @param $money
     * @param $sugars
     * @param $extraHot
     */
    public function __construct(string $money,int $sugars, string $extraHot )
    {
      $this->price = 0.4;
      $this->drinkType = 'tea';
      $this->money = $money;
      $this->sugars = $sugars;
      $this->extraHot = $extraHot;
    }


}