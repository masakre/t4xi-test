<?php

namespace Trial\CoffeeMachine\Http\Controllers;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Trial\CoffeeMachine\Http\Services\OrderGeneratorService;
use Trial\CoffeeMachine\Http\Services\OrderResponseService;
use Trial\CoffeeMachine\Http\Services\OrderValidatorService;
use Trial\CoffeeMachine\Infrastructure\OrderRepository;

class CoffeeMachineOrderController
{

    public function test( Request $request )
    {
        return new JsonResponse( $request->query->all(), Response::HTTP_OK );
    }


    public function store( Request $request )
    {
        try {
            $order          = OrderGeneratorService::generate( $request );
            $responseMsg    = OrderValidatorService::validate( $order );
            $responseStatus = Response::HTTP_BAD_REQUEST;

            // there are no errors ...
            if ( empty($responseMsg) ) {
                $responseMsg    = OrderResponseService::generate( $order );
                $responseStatus = Response::HTTP_OK;

                try {

                    OrderRepository::store($order);

                } catch (\Exception $exception) {
                    $responseMsg    = $exception->getMessage();
                    $responseStatus = Response::HTTP_BAD_REQUEST;
                }
            }

            return new JsonResponse( ( [ 'message' => $responseMsg ] ), $responseStatus );

        } catch ( \Exception $exception ) {

            return new JsonResponse(  ['message' => $exception->getMessage() ] , Response::HTTP_BAD_REQUEST );
        }

    }


}