<?php


namespace Trial\CoffeeMachine\Http\Services;


use Symfony\Component\HttpFoundation\Request;
use Trial\CoffeeMachine\Entity\AbstractOrder;
use Trial\CoffeeMachine\Entity\TeaOrder;
use Trial\CoffeeMachine\Entity\CoffeeOrder;
use Trial\CoffeeMachine\Entity\ChocalateOrder;

class OrderGeneratorService
{

    static public function generate(Request $request) : AbstractOrder
    {
        $params = $request->request->all();

        switch ( $params['drinkType'] ) {
            case 'tea':
                return new TeaOrder( $params['money'], $params['sugars'], $params['extraHot'] );

            case 'coffee':
                return new CoffeeOrder( $params['money'], $params['sugars'], $params['extraHot'] );

            case 'chocolate':
                return new ChocalateOrder( $params['money'], $params['sugars'], $params['extraHot'] );

            default:
                throw new \Exception( 'The drink type should be tea, coffee or chocolate.' );
        }
    }

}