<?php


namespace Trial\CoffeeMachine\Http\Services;


use Trial\CoffeeMachine\Entity\AbstractOrder;

class OrderResponseService
{
    static public function generate( AbstractOrder $order) {
        $orderType      = 'You have ordered a ' . $order->getDrinkType();
        $stickIncluded  = ( $order->getSugars() > 0 ) ? ' with ' . $order->getSugars() . ' sugars (stick included)' : '';
        $extraHot       = ( $order->getExtraHot() != 0) ? ' extra hot' : '';

        return $orderType . $extraHot . $stickIncluded;
    }
}