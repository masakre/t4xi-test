<?php


namespace Trial\CoffeeMachine\Http\Services;

use Trial\CoffeeMachine\Entity\AbstractOrder;

class OrderValidatorService
{

    static private $validDrinkTypes = [ 'tea', 'coffee', 'chocolate' ];
    static private $maxSugars = 2;


    public static function validate( AbstractOrder $order ): string
    {
        // check valid drinkType
        if ( !in_array( $order->getDrinkType(), self::$validDrinkTypes ) ) {
            return 'The drink type should be tea, coffee or chocolate.';
        }

        // check valid money
        if ( (float)$order->getPrice() > (float)$order->getMoney() ) {
            return 'The '. $order->getDrinkType() . ' costs ' . $order->getPrice() . '.';
        }

        // check valid sugars
        if ( $order->getSugars() < 0 || $order->getSugars() > self::$maxSugars ) {
            return 'The number of sugars should be between 0 and 2.';
        }

        return '';
    }
}