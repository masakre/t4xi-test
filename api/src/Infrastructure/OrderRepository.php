<?php


namespace Trial\CoffeeMachine\Infrastructure;


use Trial\CoffeeMachine\Entity\AbstractOrder;

class OrderRepository implements OrderRepositoryInterface
{

    public static function store( AbstractOrder $order ): bool
    {
        $dbEngine = MysqlPdoClient::getPdo();

        $stmt = $dbEngine->prepare( 'INSERT INTO orders 
                (drink_type, sugars, stick, extra_hot) VALUES 
                (:drink_type, :sugars, :stick, :extra_hot)' );

        $stmt->bindValue( ':drink_type', $order->getDrinkType() );
        $stmt->bindValue( ':sugars', $order->getSugars() );
        $stmt->bindValue( ':stick', $order->getStick() );
        $stmt->bindValue( ':extra_hot', (int)$order->getExtraHot() );

        return $stmt->execute();
    }
}