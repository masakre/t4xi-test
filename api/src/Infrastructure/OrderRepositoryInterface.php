<?php


namespace Trial\CoffeeMachine\Infrastructure;


use Trial\CoffeeMachine\Entity\AbstractOrder;

interface OrderRepositoryInterface
{
    public static function store(AbstractOrder $order): bool;
}