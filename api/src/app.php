<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Trial\CoffeeMachine\Http\Services\NamingService;
use Trial\CoffeeMachine\Http\Services\OrderGeneratorService;
use Trial\CoffeeMachine\Http\Services\OrderValidatorService;
use Trial\CoffeeMachine\Infrastructure\MysqlPdoClient;
use Trial\CoffeeMachine\Infrastructure\OrderRepository;


$app = new Application();
$app['debug'] = true;

// middlewares
$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});


$app->after(function (Request $request, Response $response) {
    $response->headers->set('Access-Control-Allow-Origin', '*');
    $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, HEAD');
    $response->headers->set('Access-Control-Allow-Headers', 'Authorization, Origin, X-Requested-With, Content-Type, Accept');
});

//$app->options("{anything}", function () {
//    return new JsonResponse(null, 204);
//})->assert("anything", ".*");
//
//
//$app->get('/orders', function (Request $request) use ($app) {
//    return new Response(json_encode( $request->get('foo') ));
//});

$app->get('/', function (Request $request) use ($app) {
    return new Response('I am a coffee-machine! Order me!!');
});

$orders = $app['controllers_factory'];

$orders->get('/test', "\Trial\CoffeeMachine\Http\Controllers\CoffeeMachineOrderController::test");
$orders->post('', "\Trial\CoffeeMachine\Http\Controllers\CoffeeMachineOrderController::store");
$app->mount('/orders', $orders);

return $app;
