import { HttpClientModule } from '@angular/common/http';
import { BrowserModule, By } from '@angular/platform-browser';
import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { Order } from './models/order';
import { of } from 'rxjs';
import { OrderService } from './services/order.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        BrowserModule,
        HttpClientModule
      ],
      providers: [
        { provide: OrderService, useClass: FakeOrderService }
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should have as title "coffee-machine"', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('coffee-machine');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('coffee-machine is running!');
  });

  it('should render 5 card containers', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();

    const cardContainerElems = fixture.debugElement.queryAll(By.css('.card-container'));

    expect(cardContainerElems.length).toBe(5);
  });

  it('should render 13 cards', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();

    const cardContainerElems = fixture.debugElement.queryAll(By.css('.card'));

    expect(cardContainerElems.length).toBe(13);
  });

  it('should render 1 terminal element', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();

    const cardContainerElems = fixture.debugElement.queryAll(By.css('.terminal'));

    expect(cardContainerElems.length).toBe(1);
  });


  it('should update drinkType when `tea` drinkType button clicked', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const orderKey = 'drinkType';
    const keyValue = 'tea';
    const btn = fixture.debugElement.query(By.css(`[data-order-${orderKey}="${keyValue}"]`));
    btn.nativeElement.click();

    fixture.detectChanges();

    expect(fixture.componentInstance.order[orderKey]).toBe(keyValue);
  });


  it('should update drinkType when `coffee` drinkType button clicked', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const orderKey = 'drinkType';
    const keyValue = 'coffee';
    const btn = fixture.debugElement.query(By.css(`[data-order-${orderKey}="${keyValue}"]`));
    btn.nativeElement.click();

    fixture.detectChanges();

    expect(fixture.componentInstance.order[orderKey]).toBe(keyValue);
  });


  it('should update drinkType when `chocolate` drinkType button clicked', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const orderKey = 'drinkType';
    const keyValue = 'chocolate';
    const btn = fixture.debugElement.query(By.css(`[data-order-${orderKey}="${keyValue}"]`));
    btn.nativeElement.click();

    fixture.detectChanges();

    expect(fixture.componentInstance.order[orderKey]).toBe(keyValue);
  });


  it('should update sugars when `0` sugars button clicked', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const orderKey = 'sugars';
    const keyValue = 0;
    const btn = fixture.debugElement.query(By.css(`[data-order-${orderKey}="${keyValue}"]`));
    btn.nativeElement.click();

    fixture.detectChanges();

    expect(fixture.componentInstance.order[orderKey]).toBe(keyValue);
  });


  it('should update sugars when `1` sugars button clicked', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const orderKey = 'sugars';
    const keyValue = 1;
    const btn = fixture.debugElement.query(By.css(`[data-order-${orderKey}="${keyValue}"]`));
    btn.nativeElement.click();

    fixture.detectChanges();

    expect(fixture.componentInstance.order[orderKey]).toBe(keyValue);
  });


  it('should update sugars when `2` sugars button clicked', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const orderKey = 'sugars';
    const keyValue = 2;
    const btn = fixture.debugElement.query(By.css(`[data-order-${orderKey}="${keyValue}"]`));
    btn.nativeElement.click();

    fixture.detectChanges();

    expect(fixture.componentInstance.order[orderKey]).toBe(keyValue);
  });


  it('should update extraHot when `0` extraHot button clicked', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const orderKey = 'extraHot';
    const keyValue = 0;
    const btn = fixture.debugElement.query(By.css(`[data-order-${orderKey}="${keyValue}"]`));
    btn.nativeElement.click();

    fixture.detectChanges();

    expect(fixture.componentInstance.order[orderKey]).toBe(keyValue);
  });


  it('should update extraHot when `1` extraHot button clicked', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const orderKey = 'extraHot';
    const keyValue = 1;
    const btn = fixture.debugElement.query(By.css(`[data-order-${orderKey}="${keyValue}"]`));
    btn.nativeElement.click();

    fixture.detectChanges();

    expect(fixture.componentInstance.order[orderKey]).toBe(keyValue);
  });


  it('should update money when `0.5` money button clicked', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const orderKey = 'money';
    const keyValue = 0.5;
    const btn = fixture.debugElement.query(By.css(`[data-order-${orderKey}="${keyValue}"]`));
    btn.nativeElement.click();

    fixture.detectChanges();

    expect(fixture.componentInstance.order[orderKey]).toBe(keyValue);
  });


  it('should update money when `coffee` money button clicked', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const orderKey = 'money';
    const keyValue = 1;
    const btn = fixture.debugElement.query(By.css(`[data-order-${orderKey}="${keyValue}"]`));
    btn.nativeElement.click();

    fixture.detectChanges();

    expect(fixture.componentInstance.order[orderKey]).toBe(keyValue);
  });


  it('should reset order to orderDefaultSettings when Clear Btn is clicked', () => {
    const fixture = TestBed.createComponent(AppComponent);
    // random order settings
    fixture.componentInstance.order = {
      drinkType: 'tea',
      extraHot: 0,
      sugars: 1,
      money: 0.5
    };

    fixture.detectChanges();

    const orderKey = 'btn';
    const keyValue = 'clear';
    const btn = fixture.debugElement.query(By.css(`[data-order-${orderKey}="${keyValue}"]`));
    const defaultOrderSettings = fixture.componentInstance.orderService.emptyOrderSettings;

    btn.nativeElement.click();

    fixture.detectChanges();

    expect(fixture.componentInstance.order.drinkType).toBe(defaultOrderSettings.drinkType);
    expect(fixture.componentInstance.order.sugars).toBe(defaultOrderSettings.sugars);
    expect(fixture.componentInstance.order.extraHot).toBe(defaultOrderSettings.extraHot);
    expect(fixture.componentInstance.order.money).toBe(defaultOrderSettings.money);
  });


  it('should call orderService.add when Submit Btn is clicked', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    spyOn(fixture.componentInstance, 'onOrderClick');
    spyOn(fixture.componentInstance.orderService, 'add');
    const orderKey = 'btn';
    const keyValue = 'submit';
    const btn = fixture.debugElement.query(By.css(`[data-order-${orderKey}="${keyValue}"]`));

    btn.nativeElement.click();
    fixture.detectChanges();

    expect(fixture.componentInstance.onOrderClick).toHaveBeenCalled();
    // expect(fixture.componentInstance.orderService.add).toHaveBeenCalled();
  });


  it('should show "Please select all options" when coffee machine has empty values ', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();

    let orderKey;
    let keyValue;
    let btn;
    const btnSubmit = fixture.debugElement.query(By.css(`[data-order-btn="submit"]`));

    btnSubmit.nativeElement.click();

    fixture.detectChanges();

    expect(fixture.componentInstance.terminalMessage).toBe('Please select all options');

    // click tea button
    orderKey = 'drinkType';
    keyValue = 'tea';
    btn = fixture.debugElement.query(By.css(`[data-order-${orderKey}="${keyValue}"]`));
    btn.nativeElement.click();

    fixture.detectChanges();

    btnSubmit.nativeElement.click();

    fixture.detectChanges();
    expect(fixture.componentInstance.terminalMessage).toBe('Please select all options');

    // click 1 sugar button
    orderKey = 'sugars';
    keyValue = '1';
    btn = fixture.debugElement.query(By.css(`[data-order-${orderKey}="${keyValue}"]`));

    btn.nativeElement.click();

    fixture.detectChanges();

    btnSubmit.nativeElement.click();
    fixture.detectChanges();

    expect(fixture.componentInstance.terminalMessage).toBe('Please select all options');

    // // click extrahot 0 button
    orderKey = 'extraHot';
    keyValue = '0';
    btn = fixture.debugElement.query(By.css(`[data-order-${orderKey}="${keyValue}"]`));

    btn.nativeElement.click();

    fixture.detectChanges();

    btnSubmit.nativeElement.click();
    fixture.detectChanges();

    expect(fixture.componentInstance.terminalMessage).toBe('Please select all options');

    // // click money 0 button
    orderKey = 'money';
    keyValue = '0.5';
    btn = fixture.debugElement.query(By.css(`[data-order-${orderKey}="${keyValue}"]`));

    btn.nativeElement.click();

    fixture.detectChanges();

    btnSubmit.nativeElement.click();
    fixture.detectChanges();

    expect(fixture.componentInstance.terminalMessage).toBeFalsy();

  });


});


class FakeOrderService {

  emptyOrderSettings: Order = {
    drinkType: '',
    extraHot: -1,
    money: 0,
    sugars: -1
  };

  validValues = {
    drinkType: ['tea', 'coffee', 'chocolate'],
    sugars: [0, 1, 2],
    extraHot: [0, 1],
    money: [0.5, 1]
  };

  add(order: Order) {
    return of([]);
  }

  hasEmptyOrderSettings(order: Order): string {

    return Object.keys(this.emptyOrderSettings)
      .find(key => this.emptyOrderSettings[key] === order[key]);

  }
}
