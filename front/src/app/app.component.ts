import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { OrderService } from './services/order.service';
import { Order } from './models/order';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  order: Order;
  title = 'coffee-machine';
  terminalMessage = 'Order!!';

  constructor(private http: HttpClient, public orderService: OrderService) {
    this.order = { ...this.orderService.emptyOrderSettings };
  }


  onOrderClick() {

    if (!!this.orderService.hasEmptyOrderSettings(this.order)) {
      this.terminalMessage = 'Please select all options';
      return;
    }

    this.terminalMessage = '';

    this.orderService.add(this.order)
      .subscribe((data: any) => {
        this.terminalMessage = data.message;
      },
        (error) => {
          this.terminalMessage = error.message + ' Try again!';
        }
      );
  }


  onClearClick() {
    this.order = { ...this.orderService.emptyOrderSettings };
  }


}
