export interface Order {
  drinkType: string;
  money: number;
  sugars: number;
  extraHot: number;
}
