import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { OrderService } from './order.service';
import { Order } from '../models/order';

describe('OrderService', () => {

  let service: OrderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [OrderService]
    });

    service = TestBed.get(OrderService);
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('should find empty order keys', () => {
    const emptyOrder: Order = { ...service.emptyOrderSettings };
    expect(service.hasEmptyOrderSettings(emptyOrder)).toBe('drinkType');

    emptyOrder.drinkType = 'tea';
    expect(service.hasEmptyOrderSettings(emptyOrder)).toBe('extraHot');

    emptyOrder.extraHot = 0;
    expect(service.hasEmptyOrderSettings(emptyOrder)).toBe('money');

    emptyOrder.money = 0.5;
    expect(service.hasEmptyOrderSettings(emptyOrder)).toBe('sugars');

    emptyOrder.sugars = 1;
    expect(service.hasEmptyOrderSettings(emptyOrder)).toBeUndefined();
  });

});
