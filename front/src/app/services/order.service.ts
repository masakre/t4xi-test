import { Injectable } from '@angular/core';
import { Order } from '../models/order';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})


export class OrderService {

  protected API_HOST = 'http://localhost:9080';

  emptyOrderSettings: Order = {
    drinkType: '',
    extraHot: -1,
    money: 0,
    sugars: -1
  };

  validValues = {
    drinkType: ['tea', 'coffee', 'chocolate'],
    sugars: [0, 1, 2],
    extraHot: [0, 1],
    money: [0.5, 1]
  };

  constructor(private http: HttpClient) { }

  test() {
    return this.http.get(`${this.API_HOST}/orders/test`);
  }

  add(order: Order) {
    console.log('sending order to api', { order });
    return this.http.post(`${this.API_HOST}/orders/`, { ...order });
  }

  hasEmptyOrderSettings(order: Order): string {
    return Object.keys(this.emptyOrderSettings)
      .find(key => this.emptyOrderSettings[key] === order[key]);

  }
}
